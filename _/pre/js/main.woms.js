$( document ).ready(function() {
    console.log( "document ready!" );
  
  	$('#wp-main-nav > ul').addClass('nav navbar-nav');
   /*********************************************** 
    *	DEFINITIONS 
	***********************************************/

    var height_to_fade = $("#header").height() - 10;
    var searchWidth = $('#searchGrows').width();
    var easterEgg = 0;

   /*********************************************** 
    *	Script fades header when scrolling
	***********************************************/

    $(window).on("scroll", function(e) {
       
		if ($(window).scrollTop() >= height_to_fade){
			$("#header").fadeOut(500);
			$('.navbar-fixed-top').css({ 
				top: '0px',
				WebkitTransition : 'opacity 2s ease-in-out',
			    MozTransition    : 'opacity 2s ease-in-out',
			    MsTransition     : 'opacity 2s ease-in-out',
			    OTransition      : 'opacity 2s ease-in-out',
			    transition       : 'opacity 2s ease-in-out'
			});
		} else {
			$("#header").fadeIn(500)
			$('.navbar-fixed-top').css({ 
				top: '100px'
			});
		}

	});

	/*********************************************** 
    *	.searchGrows
	***********************************************/
	
	$('#searchGrows').bind('blur', function () {
    	$('#searchGrows').css('width', searchWidth);
    });

    $('#searchGrows').bind('focus', function () {
    	$('#searchGrows').css({
    		width 			 : searchWidth * 2.5,
    		WebkitTransition : 'width .5s ease-in-out',
			MozTransition    : 'width .5s ease-in-out',
			MsTransition     : 'width .5s ease-in-out',
			OTransition      : 'width .5s ease-in-out',
			transition       : 'width .5s ease-in-out'
    	});
    });

   /*********************************************** 
    *	Script sizes footer depending on content
	***********************************************/
	
	$('#main').css("padding-bottom", $('#footer').height());
	$('#footer').css("margin-top", -$('#footer').height());

	/*********************************************** 
    *	Script main depending on content
	***********************************************/

	// function setMainHeight(val) {
	//  	$('#main').css("height", val); 
	// }
 //    function getMainHeight() { 
 //    	//return $(window).height() - $('#header').height() - $('#footer').height() - $('.navbar-fixed-top').height(); 
 //    	return $(window).height();
 //    }
	// var main_height = getMainHeight();

	// setMainHeight(main_height);

 //    $( window ).resize(function() {
	//   	main_height = getMainHeight();
	//   	setMainHeight(main_height);
	// });

   /*********************************************** 
    *	Script sets options for twbs carousel
	***********************************************/

	$('.carousel').carousel({
	  interval: 9000,
	  pause: true,
	  wrap: true,
	  keybord: true
	})


	/*********************************************** 
    *	Enable all tooltips
	***********************************************/
	$(function () {
	  	$('[data-toggle="tooltip"]').tooltip()
	})

	/*********************************************** 
    *	Fire Modals 
	***********************************************/
	// $('.fireModal').click(function () {
	//   	var id = $(this).attr("data-target")
	//   	console.log(id);
	// 	var i = $('id').modal();
	// 	console.log(i);
	// })

	// $('.wysihtml5').wysihtml5({
	// 	"font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
	// 	"emphasis": false, //Italics, bold, etc. Default true
	// 	"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
	// 	"html": false, //Button which allows you to edit the generated HTML. Default false
	// 	"link": false, //Button to insert a link. Default true
	// 	"image": false, //Button to insert an image. Default true,
	// 	"color": false //Button to change color of font  
	// });


	/*********************************************** 
    *	wysihtml5
	***********************************************/
	// $('.wysihtml5').wysihtml5({
	//   toolbar: {
	//     "font-styles": false, // Font styling, e.g. h1, h2, etc.
	//     "emphasis": true, // Italics, bold, etc.
	//     "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
	//     "html": false, // Button which allows you to edit the generated HTML.
	//     "link": true, // Button to insert a link.
	//     "image": false, // Button to insert an image.
	//     "color": false, // Button to change color of font
	//     "blockquote": false, // Blockquote
	//     "size": 'xs' // options are xs, sm, lg
	//   }
	// });

	/*********************************************** 
    *	select2
	***********************************************/
	// $(".select2").select2({
	// 	placeholder: "Bitte treffen Sie eine Auswahl",
	// 	width: '100%',
	// 	allowClear: true
	// });

	// $(".select2tags").select2({
	// 	tags: true,
	// 	maximumSelectionLength: 5,
	// 	placeholder: "Bitte treffen Sie eine Auswahl",
	// 	width: '100%',
	// 	allowClear: true
	// });

	/*********************************************** 
    *	EASTER EGG SECTION
	***********************************************/

	/*********************************************** 
    *	do the harlem shake
	***********************************************/
	$('.harlemshake').one("click", function(){
		//javascript:
		(function () {
		    function addStyleSheet() {
		        var e = document.createElement("link");
		        e.setAttribute("type", "text/css");
		        e.setAttribute("rel", "stylesheet");
		        e.setAttribute("href", styleSrc);
		        e.setAttribute("class", markerClass);
		        document.body.appendChild(e)
		    }

		    function removeAddedElements() {
		        var e = document.getElementsByClassName(markerClass);
		        for(var t = 0; t < e.length; t++){
		            document.body.removeChild(e[t])
		        }
		    }

		    function flashEffect(){
		        var e = document.createElement("div");
		        e.setAttribute("class", flashClass);
		        document.body.appendChild(e);
		        setTimeout(function(){
		            document.body.removeChild(e)
		        }, 100)
		    }

		    function getSize(e){
		        return {
		            height: e.offsetHeight,
		            width: e.offsetWidth
		        }
		    }

		    function isRightSize(i){
		        var s = getSize(i);
		        return s.height > minHeight && s.height < maxHeight
		               &&
		               s.width > minWidth && s.width < maxWidth
		    }

		    function heightFromTop(e) {
		        var n = 0;
		        while ( !! e) {
		            n += e.offsetTop;
		            e = e.offsetParent
		        }
		        return n
		    }

		    function getWindowHeight() {
		        var e = document.documentElement;
		        if ( !! window.innerWidth) {
		            return window.innerHeight
		        }
		        else if (e && !isNaN(e.clientHeight)) {
		            return e.clientHeight
		        }
		        return 0
		    }

		    function getPageYOffset() {
		        if (window.pageYOffset) {
		            return window.pageYOffset
		        }
		        return Math.max(document.documentElement.scrollTop, document.body.scrollTop)
		    }

		    function isOnScreen(e) {
		        var t = heightFromTop(e);
		        return t >= yOffset && t <= windowHeight + yOffset
		    }

		    function main() {
		        var e = document.createElement("audio");
		        e.setAttribute("class", markerClass);
		        e.src = mp3Src;
		        e.loop = false;
		        e.innerHTML = " <p>If you are reading this, it is because your browser does not support the audio element. We recommend that you get a new browser.</p>";
		        document.body.appendChild(e);
		        e.addEventListener("ended", function () {
		            removeAllShakeMeClasses();
		            removeAddedElements()
		        }, true);
		        e.addEventListener("canplay", function () {
		            setTimeout(function () {
		                addImFirstClass(startElement)
		            }, 500);
		            setTimeout(function () {
		                removeAllShakeMeClasses();
		                flashEffect();
		                for (var e = 0; e < O.length; e++) {
		                    addShakeMeClass(O[e])
		                }
		            }, 15500)
		        }, true);
		        e.play()
		    }

		    function addImFirstClass(e) {
		        e.className += " " + shakeMeClass + " " + imFirstClass
		    }

		    function addShakeMeClass(e) {
		        e.className += " " + shakeMeClass + " " + varClasses[Math.floor(Math.random() * varClasses.length)]
		    }

		    function removeAllShakeMeClasses() {
		        var e = document.getElementsByClassName(shakeMeClass);
		        var t = new RegExp("\\b" + shakeMeClass + "\\b");
		        for (var n = 0; n < e.length;) {
		            e[n].className = e[n].className.replace(t, "")
		        }
		    }

		    var minHeight = 30;
		    var minWidth = 30;
		    var maxHeight = 350;
		    var maxWidth = 350;
		    var mp3Src = "//s3.amazonaws.com/moovweb-marketing/playground/harlem-shake.mp3";
		    var shakeMeClass = "mw-harlem_shake_me";
		    var imFirstClass = "im_first";
		    var varClasses = ["im_drunk", "im_baked", "im_trippin", "im_blown"];
		    var flashClass = "mw-strobe_light";
		    var styleSrc = "//s3.amazonaws.com/moovweb-marketing/playground/harlem-shake-style.css";
		    var markerClass = "mw_added_css";
		    var windowHeight = getWindowHeight();
		    var yOffset = getPageYOffset();
		    var pageElements = document.getElementsByTagName("*");
		    var startElement = null;

		    // ----Starts here----
		    for (var L = 0; L < pageElements.length; L++) {
		        var A = pageElements[L];
		        if (isRightSize(A) && isOnScreen(A)) {
		            startElement = A;
		            break
		        }
		    }
		    if (startElement === null) {
		        console.warn("Could not find a node of the right size. Please try a different page.");
		        return
		    } else{
		        console.log("Found start element: ",A," with width "+getSize(A).width+", height "+getSize(A).height+", and a total Y offset of "+heightFromTop(A));
		    }
		    addStyleSheet();
		    main();
		    var O = [];
		    for (var L = 0; L < pageElements.length; L++) {
		        var A = pageElements[L];
		        if (isRightSize(A)) {
		            O.push(A)
		        }
		    }
		})()
	});

   /*********************************************** 
    *	cheet http://lou.wtf/cheet.js/
	***********************************************/
	
	// cheet('t h a l l e r', function () {
	//   	alert('Good Morning, Mr. Thaller!');
	// });

	// cheet('↑ ↑ ↓ ↓ ← → ← → b a', function () {
	//   	alert('Voilà!');
	// });

	// cheet('o n c e', function () {
	//   	console.log('This will only fire once.');
	//   	cheet.disable('o n c e');
	// });
});

