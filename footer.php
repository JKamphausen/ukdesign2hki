<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?>
    <footer id="footer" class="source-org vcard copyright">
        <slice class="footer-slice fs-fourth">
          <div class="container">
            <p>content</p>
          </div>
        </slice>
        <slice class="footer-slice fs-third">
          <div class="container">
            <p>content</p>
          </div>
        </slice>
        <slice class="footer-slice fs-second">
          <div class="container">
            <p>content</p>
          </div>
        </slice>
        <slice class="footer-slice fs-default">
          <div class="container fs-small-text">
            <p class="pull-left">
              <strong>Historisch-Kulturwissenschaftliche Informationsverarbeitung</strong><br>
              Universität zu Köln | Albertus-Magnus-Platz | D-50931 Köln<br>
            Tel.: +49 - 221 - 470 1751 | Fax: +49 - 221 - 470 1754<br>
            Email: <a href="mailto:hki-office@uni-koeln.de">hki-office@uni-koeln.de</a>

            </p>
            <p class="pull-right">
              <span id="copyright">&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></span>
              <a href="#">Sitemap</a>
              <a href="#">Impressum</a>
              <a href="#">Kontakt</a>
            </p>
          </div>
        </slice>
      </footer>

	</div>

	<?php wp_footer(); ?>


<!-- jQuery is called via the WordPress-friendly way via functions.php -->

<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>

<!-- Asynchronous google analytics; this is the official snippet.
         Replace UA-XXXXXX-XX with your site's ID and domainname.com with your domain, then uncomment to enable.

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-XXXXXX-XX', 'domainname.com');
  ga('send', 'pageview');

</script>
-->

</body>

</html>
